從塞蘭大迷宮回來的第二天，第四場考試的合格者們再次被召集到了訓練場。
然後，向我們傳達了最終考試的內容。

「你們要進行一對一的模擬賽，對手是在校生。當然，想要贏是很難的，所以，根據戰鬥的內容上來判斷是否合格。」

到這裡的人只剩下了三十二名。
其中可以合格入學的只有二十個人。

「那麼接下來，我將告知每位考生的對戰選手。」
「居然是用那樣的手段來⋯⋯」

就連艾莉婭也臉色蒼白地嘆了口氣。

剛才每位考生的對手都公布了⋯⋯她的對手，竟然是萊奧斯。

「不管怎麼想都是特意為了阻止艾莉婭入讀這所學院的吧。」

據說即使考生輸了，也不確定是否合格。
但是，進行判定的是那個考官。

那個考官未必能公平地處理結果。

萊奧斯是有名的貴族弟子。
所以扭曲考官的判斷標準，這種程度的事應該還是可以做到的。
因此，不管艾莉婭在戰鬥中展現出怎樣的精彩的表現，也肯定會做出不合格的判斷。

但是，如果考生們贏了，就不可能做出這樣的判斷了。
然後，對方如果是萊奧斯的話，也許還能夠取勝。

『小姑娘不太可能會贏那個男人吶。』

萊奧斯絶對不算弱。

我雖然勝利了、但是是因為有了這把劍。

另一方面，艾莉婭連一把正經的劍都沒有。

當然也沒有錢。

就算可以買到，也頂多是用像是老舊、失敗作的青銅製的劍吧。
但是那樣的話，和萊奧斯的劍碰撞幾次就不能用了。

剛才發布對戰選手的時候，也看到了萊奧斯的身影。
腰間掛著的不是前幾天的那把，
不愧是名門貴族，新的佩劍也是聖銀秘銀製的寶刀。

而且那把劍，大概比被我斬斷了的那把還要好啊。

這麼說來，那時說過那把劍是練慣用的二級品嗎⋯⋯

「至少能有那傢伙的劍的一半的性能就好了啊⋯⋯或者，用這傢伙的話⋯⋯」

曾經有一次，把神劍試著遞給了艾莉婭。
但是，在那一瞬間，它變得非常鈍，連稻草都砍不斷。

吶，維納斯啊，有沒有什麼好的辦法啊？

『有吶。』

是啊。

嘛，能從你那裡得到好的回答、也沒有過什麼期待──

「──誒？有嗎！？」
『喂，汝不覺得剛才自己很失禮嗎？吾可是神劍啊？希望汝能更加期待吾吶？』
「比起這些快點把方法告訴我吧。」

聽到了我追問維納斯的話後，艾莉婭帶著困惑的表情看向了我。

「我從以前就這麼想了⋯⋯你有時會跟自己的劍搭話呢？⋯⋯不，也、也不是說有什麼問題喔？一定是自己孤零零地一個人，感到寂寞了吧？我也能理解那個心情。呵呵，我也是呀，在無聊的時候會和動物說話──」
「才不是這樣的啊！」

維納斯向我傳達了『把吾遞給那個小姑娘一下』，所以我按照它說的做了。

『啊、啊、測試測試。怎麼樣？聽的到嗎？』
「誒？聲音！？難道是這把劍在說話嗎？」
『就是那樣吶！首先讓吾進行下自我介紹。吾維納斯・薇克多！是由愛與勝利的女神維妮婭所製的神之劍！』

艾莉婭聽聞後兩眼閃閃發光。

「神劍⋯⋯在古代的文獻裡有讀到過，難道是真實存在的⋯⋯好厲害！」

似乎因為祖先是剣聖，對於劍這方面似乎很了解。

「不，雖然我不清楚這個傢伙是不是真正的神劍⋯⋯」
『你在說些什麼呢！能像我這樣可以溝通的劍什麼的，是不存在其他把的！』
「比起這個，快把方法告訴我吧。」

維納斯咕姆姆～的發出了不滿的聲音。

心情差成那樣了嗎？

維納斯說著『請更加地尊敬吾吶！』就這樣把臉轉了過去。
不對，它沒有臉啊。

「拜託了維納斯桑，我也想知道那個辦法。」
『既然你都那麼說的話，那就沒辦法了～』

但是被艾莉婭拜託了後就很爽快地答應了。
真的、是把好色的劍啊。

『艾莉婭吶，對盧卡斯的「眷姫」，感興趣嗎？』
「⋯眷姫？」
『唔姆。成為「眷姫」的話，可以將「疑似神劍・維納斯的複製品」給汝使用吶。那把劍可以說是我的孩子，而且正如其名，有著能與神造武具相匹敵的程度吶。』
「真厲害！拜託了，請把我變成盧卡斯的『眷姫』吧！」
『順便說一句，「眷姫」，是和妻子一樣的東西吶。』
「妻、妻子！？」

艾莉婭吃驚的叫了出來。

『就是那樣吶！吾是由愛與勝利的女神所創造出來的神劍！所以力量的源泉就是愛！』

我從旁邊插入了對話中。

「等一下啊，我可沒聽你說過你的【固有能力】有著這樣的功能啊？」
『所以我現在才要說。隨著「眷姫」和「疑似神具」的增加。最後會組成最強的後宮軍團！這才是＜眷姫後宮＞的真正的力量！』

什麼？這是什麼鬼能力？