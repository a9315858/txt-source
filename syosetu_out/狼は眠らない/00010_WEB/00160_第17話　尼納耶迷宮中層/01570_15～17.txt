１５

隔天的晚飯時間，雷肯從艾達那收到了個報告。

「什麼？卡加爾來見妳了？」
「嗯。雷肯跟赫蕾絲小姐去賣素材的時候，來拜訪宿屋了。」

宿屋的名字，是故意不說的。雷肯思考著是在哪裡得知的，但〈尖岩〉可是這城鎮的英雄。有很多方法調查吧。

「然後就問我，要不要加入〈尖岩〉。」
「怎麼回答？」
「我說，那種事，就去找雷肯。」
「那就好。了不起。」
「誒嘿嘿嘿嘿。」
「成長了阿。」
「嘿嘿嘿嘿嘿。」


１６

再隔天，在宿屋吃完早飯後，正打算去保管了〈拉斯庫之劍〉的武器店露臉時，卡加爾來了。

「能給點時間嗎。」
「有事就在這裡說吧。」

卡加爾似乎在意著目光，瞄了瞄四周，但嘆了一小口氣後，就坐到雷肯前面的椅子上。

「能把艾達讓給老夫嗎。拜託了。」

說完便低下了頭。

「我拒絶。」

卡加爾從懷中取出金袋，交到雷肯面前。

「裡面有白金幣五枚。是老夫現在能出的極限了。」
「就說我拒絶了。」
「〈虹石〉有你在。一支隊伍不需要兩個〈大回復〉持有者吧。」
「不是由你決定的。」
「有厲害的弓使。介紹給你。」
「用在你的隊伍吧。」
「嘛，聽著。這邊的魔法使貝塔，被嚇得畏縮不已了。說是，不想再探索了。但是又說，如果跟艾達一起就戰鬥得了。」
「跟我無關。」
「貝塔她，本來是個膽怯的孩子。不過，有著非常好的魔法才能。不覺得很有趣嘛。那麼膽怯的孩子，有著千中選一的攻擊魔法的才能。老夫可費了多少工夫才把那孩子。」
「對你的經歷沒興趣。就這點事嗎。」

雷肯站了起來。

「老夫都像這樣低頭拜託了，還不肯聽嗎。」
「你只是在把自己的方便強加到別人身上罷了。是不會把艾達交給這種人的。而且不論如何，艾達現在是我的弟子。沒有師傅會出賣弟子。」

卡加爾瞪著雷肯一陣子後，憤然地走了出去。


１７

在〈傑德的店〉吃晚飯時，魔法使貝塔來了，並站在艾達旁邊。

「小艾達。」
「貝塔小姐。晚上好。要不要坐下？」
「幫幫我」
「誒？」
「只有一個人就會怕得不敢去迷宮。但是，不去迷宮就賺不了錢。我，欠卡加爾錢。很多的錢。」
「貝塔小姐⋯」
「至今都一擊就打倒了魔獸。就算一擊打不倒，第二擊也能打倒。我擊出魔法的話戰鬥就會結束。所以不可怕。」

聽了這句話，雷肯覺得可疑。從第三十一階層戰鬥後的狀況來看，〈尖石〉應該沒有用過靠必殺的一擊來解決的戰鬥方式才對。

（等等）
（因為〈迷路之龍〉也在一起，所以把本領藏起來了嗎）
（藏起了本領才被魔獸給蹂躪了是嗎）

「但是那個時候，被濺到了溶解液，臉漸漸的被融化掉。」

（那麼貝塔這女的就是〈尖石〉的王牌）

「怕的感覺要死去了。覺得就算有得救也會活不下去。」

（用這種戰鬥方式的隊伍在原本的世界也很多）

「但是，小艾達救了我。」

（湊齊魔力增幅和讓攻擊魔法的威力加倍的裝備）

「爛掉的臉變回原本完好的臉了。」

（花上充足的時間來準備魔法）

「簡直就是奇蹟。」

（在那期間就讓別的成員吸引敵人）

「就算回復師的多蘭先生還活著，也做不到那種事。」

（是一口氣屠殺敵人的戰法）

「小艾達就是女神大人」

（至少在對付強敵時會使用這種戰法）

「跟小艾達在一起的話就不可怕。戰鬥得了。」

（原來如此）

「所以拜託了。」

（要是這女魔法使派不上用場）

「跟我一起來吧。」

（隊伍的能力就會一口氣下降吧）

「很抱歉，我不能跟著一起去。我的歸屬是在雷肯身邊。」

被拒絶的貝塔，眼睛泛著淚瞪了雷肯後，便轉身走掉了。

「真是的。」

阿利歐斯嘆了口氣。

「那個叫卡加爾的人，耍了個挺心機的小手段呢。」
「既然這麼想，為什麼不把那女的趕走。」
「雷肯先生不也是嗎？」

此時赫蕾絲這麼說道。

「真的是一群自私的人阿。只會考慮自己的方便。根本沒理解，這是多麼厚顏無恥的請求吧。」

早上的卡加爾和雷肯的對話，赫蕾絲和阿利歐斯，都在離了點距離的位子上聽到了。

「赫蕾絲小姐。」
「嗯？怎麼了。」
「不。沒什麼。」

阿利歐斯是想說，您不是也一樣嗎，吧。

確實赫蕾絲也做了個自私的請求。
但是，雷肯沒有從赫蕾絲身上感到不快。

從卡加爾和貝塔，則感受到了極度的不快。
雖然想著這差異是怎麼回事，但想不出來。

但是，在思考中，注意到了一件事。
在迷宮裡時，卡加爾這麼說了。

「〈迷路之龍〉持有第十九階層的〈印〉。我們一口氣，從那裡來了這階層，以製作〈印〉。要是沒發生事故，就不會有任何問題才對。」

但是，赫蕾絲說過，到第三十一階層製作〈印〉的委託，有個條件是要持有第三十階層的〈印〉。也就是說，〈尖岩〉接下的〈迷路之龍〉的委託，並非正規的委託。
是哪一邊提案的呢。
恐怕是〈迷路之龍〉這邊。

自己一行人有第十九階層的〈印〉，有辦法請人幫忙製作第三十一階層的〈印〉嗎。是跟卡加爾商量的嗎，又或者是同伴間在討論時，被卡加爾聽到了嗎。

而卡加爾輕易地答應了。
原來如此，所以卡加爾才會拚命拜託雷肯。

（果然拒絶那個是正解阿）

本來，雷肯就不同情〈迷路之龍〉，現在更沒有同情的餘地了。
在迷宮發生的事，都是自己的責任。不想敗北或死亡的話，打從一開始就不該潛入。