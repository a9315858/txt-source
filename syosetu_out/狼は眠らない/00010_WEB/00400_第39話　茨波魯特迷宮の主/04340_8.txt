８

「請容我放棄。沒有自大到會考慮不以恩寵跟那種對手戰鬥」
「喔？」

阿利歐斯強得很厲害。而且那強大，是來自於充分鍛鍊的劍之技術和作為武人的心與戰鬥直覺。估計是在一族的歷史中積累的技術和知識成了基礎。也有阿利歐斯能跟那迷宮主戰鬥的感覺。但本人不打的話，就沒什麼好說的了。

「但是，跟那種敵人戰鬥，而且還打贏，真是僥倖呢。得到了千載難逢的經驗呢」
「但是得到的是金色藥水這點沒辦法接受。是茨波魯特迷宮的最下層的魔獸喔。是〈劍之迷宮〉的迷宮主喔」
「哈哈。雷肯殿」
「怎樣」
「對雷肯殿來說，佐爾坦先生才是迷宮之主不是嗎」
「什麼？不，但是。呼嗯」

那也說不定，雷肯這麼想。
至少，對雷肯來說，佐爾坦才正是迷宮之主。

在那強大上。在那存在感上。然後是在那報酬上。
正是如此不是嗎。

佐爾坦為雷肯留下了什麼呢。
首先，給了千載難逢的戰鬥經驗。
然後，吸收佐爾坦的強度，讓雷肯來到了新的領域。

而且還讓出了〈不死王之戒指〉。
得說是配得上大迷宮之踏破的報酬。

阿利歐斯小聲嘟嚷。

「蝙蝠魔人跟骸骨魔王嗎」
「別用那稱呼叫我」

雷肯被舒適的疲勞感和成就感籠罩。
兩人踏破了茨波魯特迷宮。
雖然想放鬆，但沒辦法。
門被粗魯地打開，布魯斯卡衝了進來。

「雷肯！迷，迷宮。茨波魯特迷宮」

雷肯默默地等布魯斯卡說下一句話。

「你，難道說，該不會」
「啊啊」
「踏，踏破了嗎？」
「啊啊」

布魯斯卡的嘴巴開開合合，雙手以奇妙的動作轉來轉去。自己也不知道想做什麼吧。

「總，總之過來吧」
「哪裡」
「迷宮廣場阿！」

說實話，出門很麻煩，但布魯斯卡很強硬。

雷肯被布魯斯卡拉到了迷宮廣場。在那裡有多得無法相信的冒険者。廣大的空間完全被人掩埋了。
自扎卡王国建国以來，茨波魯特迷宮都沒有被踏破過。魔獸會從被踏破的迷宮消失，正如字面上所說的進入休眠。這裡的冒険者們，面對這至今沒發生過，認為今後也不可能發生的事態，感到驚訝慌張。

但是，一部份的冒険者，知道蝙蝠魔人到達了百二十一階層。他們應該有想到，說不定迷宮被踏破了。
迷宮被踏破啦─，布魯斯卡大聲喊叫。怒號湧出。雷肯由於疲勞感和虛脫感而搖搖晃晃的，同時被捲進了騷動中。所幸有阿利歐斯在身邊，幫忙回答了連珠炮似的質問，雷肯只要交給他就好。

不知不覺之間，廣場成了巨大的宴會場。
酒和料理被帶進來，到處都有冒険者在把酒言歡。

「雷肯」

坐在石疊上拿著酒盃抬頭一看，領主基爾安特・諾茨站在那裡。也有領主輔佐海丹特・諾茨和迷宮事務統括官伊萊莎・諾茨的身影。騎士拜亞德・連古拉也在旁待命。全員都是徒步。果然沒辦法讓馬進入這密集著冒険者的廣場吧。

「踏破迷宮了嗎」
「啊啊」
「有幾階層」
「百五十」
「百五十階層！那麼，你們，僅僅兩個月，就從百二十一階層前進到了百五十階層嗎」
「啊啊」
「僅憑兩人來」
「啊啊」
「這實在是。你們這樣的冒険者，從沒見過也從沒聽過」
「領主大人」
「阿利歐斯殿，怎麼了嗎」
「最下層只有迷宮主的房間，那裡的限制人數是一人」
「什，麼？」
「雷肯殿靠自己戰勝了迷宮之主」

就連基爾安特也啞口無言了。

伊萊莎在雷肯前方跪下，兩手握住拿著酒盃的右手。意外地柔軟的手。手指纖細又美麗。

「雷肯殿！雷肯殿！您這個人，真的是，何等的」

眼睛浮現了淚水。想著難道是因為被搶先了而感到悲傷嗎，但重新一想，應該沒可能。

「伊萊莎。讓一下」

海丹特的聲音有點僵硬。說不定是在為迷宮被踏破感到生氣，但這種事跟雷肯無關。

「雷肯⋯殿。雖然有很多話想問，但在這麼吵鬧的場所可沒辦法談話。為表彰你的榮譽，想在領主館舉辦晚餐會。會來吧」
「看心情吧」