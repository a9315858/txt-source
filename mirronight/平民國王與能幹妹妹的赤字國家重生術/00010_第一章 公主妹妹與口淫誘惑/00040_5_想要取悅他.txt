我終於明白了。

原來這位公主之所以討厭我是這個緣故啊。

蘿絲公主本來是孤兒，能夠依賴的人只有陛下。陛下去世之後公主便子然一身了。塞巴斯汀雖知曉公主的生平，但到底也只是名家臣。

就算學習禮儀和舞蹈，成了落落大方的公主，然而面對國家的危機被迫做下決斷，卻不知如何是好。

這孩子是獨自一人在奮戰。

我能理解她的心情。

我在雙親因傳染病去世時，也因悲傷和不安交織弄得魂不守舍。

「有我陪著你，放心吧。」

這是我在父親葬禮結束後不知何去何從之時，義父所說的話。義父拍了拍我的背，

令我心中踏實不少。

我抱緊妹妹拍了拍哭得淚人兒般的她。

軟綿綿的身體緊貼著我，能清楚感受到她的溫度還聞到一股甜美的體香，而柔亮的頭髮好像散發光芒一樣。

仰望站在王宮露台揮手的公主時只覺她的身姿端莊大方，沒想到實際上是個如此怜人的女孩子。

糟糕身體起反應了。

「王兄腰那邊好像腫起來了。」

我趕緊把手放在妹妹肩上將她推開。

「我沒事。」

「我好擔心，讓我看看吧？」

「就跟你說過沒事了！」

我將蘿絲公主伸過來的手揮開，卻被她瞪了，那簡直是看著垃圾的眼神。

「我可是在擔心你啊？你應該感到光榮才對！」

她將下巴揚起，雙手捏腰說道。那壓倒性的眼力令人無法拒絕。

「我感到十分榮幸。」

「我要摸了，可以嗎！？」

「是。」

蘿絲撫摸了兄長的下體。

「這是什麼？好熱，還不停脈動，摸起來堅硬又有些柔軟。」

「唔嗚......」

平時難以捉摸的兄長，現在卻不停顫抖著。

(呵呵，王兄竟然會如此動搖。)

當指尖撫摸到將褲子向前撐大的棒狀物，她猛然驚覺。

男人下體的東西。這難道......

(難道這個，就是女僕們竊竊私語時常提到的，雞雞嗎？)

我急忙將手收回。

「呀、難道說，這個奇怪？怎、怎麼會......變得這麼大......」

在過去乞討時，曾經看過同為孤兒的男性性器官。不過，那只有蘆筍般大小，甚至覺得有些討喜。

「這就是妳想的......那個。只要一興奮......嗚、就會......變大......」

「實在難以置信。」

「我也、不敢相信......蘿絲公主，是我們憧憬的......公主殿下......我太高興才......」

兄長的聲音顯得十分興奮。

(憧憬？我嗎？)

(沒錯，我非常美麗。是國民們向往的存在。這都是我努力的成果。)

「王兄被我摸感到高興嗎？」

「是啊......」

「知道了，那我就繼續吧。直接摸可以嗎？」

「這......」

「真是的！不要畏畏縮縮的，實在有夠煩人！！我要摸了聽到沒！」

蘿絲一鼓作氣把兄長的褲子扯下。

「呀啊！」

男根從褲子蹦出晃來晃去。

——這是什麼？這......？好可怕......

真不甘心竟被這種東西嚇到。不過身為海德堡王族的公主，無論何時都必須處之泰然才行。

維塞將身子縮成一團，雙手遮住下體。

「把手拿開！」

這名公主年紀雖比我還小卻充滿威嚴，令人不得不從。

我只好戰戰兢兢地把手放下。

「唔......！」

蘿絲公主臉頰泛紅，兩眼直盯著那東西俗雙唇不由自主地顫抖著。

「這麼噁心的東西，不要讓我看啊——！」

她竟然對我發火，太不講理了。

妹妹八成是沒看過成年男子的陰莖吧，又或者是完全沒有性知識。

畢竟她住在王宮，成天學習禮儀之類的東西。之所以生氣，或許是內心的恐懼和好奇心交戰後的表現。

正當我鬆一口氣想把褲子穿上，她又再次揚起下巴，一臉傲慢地說:

「剛才不就說了.....讓、讓我摸啊！」

(那又要我別給妳看？)

(雖說我也想讓她摸啦。)

「想要我摸還不開口請求！」

「拜託妳了。」

蘿絲公主跪在地上伸出白皙的手，以綿軟纖細的手指纏住肉莖。為了不正眼瞧見而別過頭的模樣十分可愛。

「討厭好噁心......本公主可是特別為你做這種事要感到榮幸啊！」

(呃一 現在到底是什麼情況？)

蘿絲公主一面生氣小手卻不忘緊握又鬆開眼前的東西。

「那個......能夠前後套弄嗎？」

維塞委婉地提了請求。

「像這樣？」

卷成筒狀的手前後動了起來感覺非常舒服。雖說和娼婦的手交相比自然是毫無技術可言然而那小心翼翼的動作卻產生了超乎想像的快感。

「嗚......」

「會痛嗎？」

蘿絲嚇得急忙把手鬆開。

「不非常舒服......妳是一國的公主......過去只能從遠處仰望的蘿絲公主......竟然為我做這種事，簡直像在做夢......」

「呵呵好開心。那麼我繼續喔。

公主再次握住肉莖手前後來回地搓弄。前端小孔涌出了透明液體就像是被公主搾出來似的。

「這是什麼？」

「只要舒服就會流出來。」

「是嗎？」

(這麼說來我也一樣在手淫時覺得舒服就會流出黏黏的液體。)

蘿絲將頭抬起表情變得較為柔和。

公主的職責就是成為國民憧憬的存在並常施慈愛。她從小就被如此教育或許這麼做對她而言也是相同的行為。

「呃，口交要怎麼做？」

「為什麼妳會知道這種東西？」

「我聽女僕講的，她經常告訴我要如何取悅情人。雖然我聽不太懂，總之先記了下來......譬如該怎麼舔之類的。」

「是、是這樣啊。那能幫我舔嗎？」

「舔什麼？」

「雞雞。」

「不要！好噁心——！ 我的寶貝一瞬間垂頭喪氣了。」

「呀啊這、這是怎麼！」

「那個因為被妳說噁心，該怎麼說，覺得有點受傷了......」

「這、這樣啊對不起。那麼......我要舔了。」

蘿絲希望取悅兄長，便伸出舌頭，舔了龜頭一下。

「嗚啊......」

維塞不禁發出呻吟。

過去高不可攀的蘿絲公主，竟然跪在他面前舔弄著他的龜頭。

由於她只身著晚袍再披了件睡衣乳溝清晰可見。

加上蘿絲公主的舌頭如貓一般粗粗的這兩種強烈的刺激使得維塞好像隨時都會射精。

「沒有味道啊一點也不甜。讓它變美味點！」

(這公主到底在胡說什麼啊。)

「別強人所難了。」

「這個明明像糖漿一樣透明漂亮。」

蘿絲公主抬起頭來，露出可愛的笑容。

態度軟化了？剛才明明還覺得噁心。

「能含進去嗎？」

我誠惶誠恐地嘗試問問看蘿絲公主用著朦朧的神情點了點頭。接著張開嘴把整個龜頭含了進去。

維寒從上俯瞰著夢絲公主確認到自己的男根消失在妹妹美麗的臉龐時感到無比興奮。

濕滑熾熱的舌頭緊緊纏住了龜頭。

「唔嗚......」

(這是怎麼回事怎麼會如此舒服。)

蘿絲公主的口交並沒有娼婦那樣的技術。就只是單調地吸吮舔弄而已。

但卻舒服得令人難以置信，就好像龜頭要融化在她口中。

(這孩子竟然能露出如此煽情的表情啊。)

「啾、啾噗......❤啾嚕啾嚕▽咧嚕」

身為公主充滿慈愛的笑容、哭泣時的面容、厭惡地狠瞪維塞時的表情、充滿神情，還有雙頰泛紅，含著自己肉棒時的臉。

光是想像就讓人頭暈目眩。

(這世上只有我，能看到她這麼色情的一面。)

「嗚、咕......！」

維塞忍不住發出喘息，快要射精了。

蘿絲公主眼睛朝上一望。

(圖片006)

「像這樣嗎？只要這麼做王兄就會覺得舒服？」她的表情似乎如此訴說著。

「咧嚕......嗯......哦......啾啾啾......啾噗......」

公主開始發出聲音吸吮著龜頭。

「嗚哇、嗚啊啊。」

要被吸出來了，簡直像是硬把精液從陰囊中吸出來。

腰不由自主為這股強烈的吸引感發出顫抖。不行了，已經無法忍耐，腰的深處好熱。

「蘿絲公主，等、等等......」

一陣酥麻竄過身體。

射在女孩子嘴裡這種事，怎麼想都太過失禮了。

「要射了！」

本想急忙推開她，但蘿絲公主卻雙手環住我的屁股，而且吸吮得更加賣力。

我終於明白，這位公主幾乎沒有性知識。以為我是在抗拒她，才會變本加厲地進行口淫服務。

「啾噗啾噗❤啾嚕嚕......咧嚕❤啾♥啾啾啾......」

嘟噗

「嗚哇，要、要射了......抱、抱歉！！」

嘟噗嘟噗！

蘿絲公主被嚇得兩眼睜大，急忙把肉棒吐出。

嗶啾、嗶啾嗶啾！

精液如雨下般濺在妹妹臉上。

維塞心裡慌成一團，想著怎麼能弄髒女孩子的臉。然而玷污美麗事物特有的快感，使得他腦袋一時轉不過來。

「對、對不起！」

蘿絲公主眉頭緊蹙不悅地別過頭去，臉頰上充滿方才釋放的白濁液。

她生氣了，而且是怒不可抑。

雖說剛射完整個人神清氣爽，身體卻嚇得不敢動彈。

射精總算停歇。

蘿絲公主從口袋取出手帕，靜靜地擦拭臉龐。

然後，狠狠地瞪了維塞一眼。

「對不起，那個......該怎麼說......」

妹妹轉身走出國王房間。

留維塞一人抱頭苦惱。

(我的天啊，我到底做了什麼好事。這下絕對被討厭了，說不定再也不會跟我說話......)
