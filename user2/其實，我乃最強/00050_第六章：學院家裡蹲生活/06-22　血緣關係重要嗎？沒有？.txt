驅趕野獸的竹製【添水】的敲擊聲在浴室裏迴響。

「哇～～」（夏洛特的感覺很舒服的聲音）
「呼……」（伊利斯菲利亞的恍惚歎息）
「哈……」（瑪麗安的豔麗歎息）
「咕嘟咕嘟咕嘟咕嘟咕嘟……」（芙蕾沉入水中的聲音）

在瀑布修行的地方的女子四人組，在蒼天之下泡著露天溫泉。
用板壁隔開的對面還有哈特和萊斯。順便一提，哈特隱藏了左胸的王紋。沒有曝露出來。

瑪麗安最初猶豫著初次在野外泡溫泉的行為，但是其他三個人都很放得開，所以下定了決心。

「溫泉的溫度剛好啊。而且，感覺皮膚很光滑。」

從略顯白濁的熱水中伸出柔軟的手臂，用另一隻手撫摸。
夏洛特並沒有仔細觀察她的行為，而是一直盯著公主的胸口。果然是浮起來了嗎？

另一方面，這兩個人沒有注意到，好像也有下沉的東西。

「喂，芙蕾，沒事吧？」

伊利斯菲利亞將沉入水底的芙蕾拉了起來，讓她靠在浴池邊緣。
在這裡，夏洛特也將其他人看在眼裡。
那兩人的豐腴果實，被重力玩弄著，撲通撲通撲通地跳。

（即使是母親也不會輸。總有一天我也會像母親一樣……）

但是，但是。
用手捂著自己的胸口，想著自己到底能不能到達那個領域而顫抖。

輕輕地搖頭，將不好的想法從腦海中一掃而光。
然後在熱水裏泡了一會兒，就這樣和樂融融地說了「吱莉莉」。

「那麼，我有件事想告訴大家，或者說我想告訴瑪麗安公主和萊斯王子。」

被指名的瑪麗安表情緊張起來。

「學院現在有幕後學生會的組織在暗中活動。情況危急。」

「哈？」

「……」

瑪麗安發出失態的聲音，從板壁的對面傳來了驚訝的氣氛。

「那個……夏洛特？什麼是幕後學生會……？」

「是的，實際上是『號碼』的學生集團想顛覆表面的學生會，奪取學院！」

「誒？」
「什麼？」

「而且還有一個暗中操縱他們的巨大組織。這就是企圖讓魔神復活，並支配世界的路西法拉教團！」

「那！？」
「那！？」

剛開始難以理解其含意的瑪麗安和萊斯，被出示出教團的名字，就不能將其認定為妄想。

順便一提，雖然哈特不知何時脫口說出魔神的事，但夏洛特卻以微薄的情報，甚至想像出教團的真正目的。

「魔神是什麼？」

「這是神話時代的黑暗墮落的神。非常糟糕。」

特別受到衝擊的是萊斯。
他撿起隔著木板牆壁的對面的聲音（哈特張開了稍微大點的結界），一邊泡在熱水裏一邊顫抖著。

「不會吧，媽媽她……」

先不論魔神之類的不確定的東西，傳說王妃吉澤洛特正在向教團提供資金援助。
如果教團企圖支配世界的話，可以看出母親想利用這個手段奪取王國的意圖。

「喂，夏洛特。妳到底想做什麼？」

「首先要揭露號碼們的真實身份，讓他們重新做人。因為不能做壞事」

「然後呢？」

「那個時候教團會注意到吧。我們卡美洛的存在！」

「這樣很好。你們難道不想和教團扯上關係嗎？」

看著旁邊的哈特。
哈特和我無關，一直泡在溫泉裏。
夏洛特回復精神。

「當然啦！根據一種說法，剛才的王都騷亂事件也是教團主導的。因為不能再容許這種壞事了！」

「真的嗎……」

我不想相信。
但是夏洛特的話完全是謊言，如果不是妄言之類的話，就可以理解了。

在王都騷亂事件中，萊斯本人也陷入了困境。如果走錯一步，就會被食屍鬼長老咬住，化為活著的屍體。
假如母親也參與其中的話，那麼就沒有準備任何讓兒子回避危險的手段了。
不，也許——。

（那個時候，妳想要把我和姐姐一起排除掉嗎……？）

出生以來就沒有和母親像普通的母子一樣接觸過。
自己成為下一個國王，母親作為後盾君臨天下。但一直有著這樣的疑問：「只不過是為了這個目的而生的棋子而已。」

（我已經不是棋子了嗎……）

萊斯咬緊牙根。

另一方面，在女浴池裏。

「吶，總覺得那邊的熱水裏好像有種嚴肅的氣氛放出來，但這是什麼？」

夏洛特在提心吊膽。
瑪麗安猶豫地說。

「宗教團體中流傳著吉澤洛特王妃提供資金援助的臆測。就我在王宮的感覺來說，也是這樣吧。」

「呵呵？」

伊莉絲菲利亞代替令人驚訝的夏洛特做出了回應。

「這樣的話是必然的，總有一天會和閃光公主相見吧。」

「哇啊……」

根據情況的不同，親子也會互相殘殺。

「這樣的展開在動畫裏也經常有，不過，如果變成現實的話，就不合適了……」

面對突如其來的嚴肅難題，夏洛特的頭開始過熱。

「沒事，沒什麼。」

哈特說出真心話。

「血緣關係是一件微不足道的事。只是碰巧擋住的敵人是親生父母而已。不必客氣，把它毀掉就好了」

萊斯把視線轉移到了哈特身上。
雖然沉浸在浴池裏心情很好，但是這話裏也包含難以言喻的「力量」。

哈特的出身還不清楚。
只不過是芬菲斯邊境伯爵說過「收養了平民的孤兒」而已。

也許，哈特的話是從實際經驗中來的。
這是事實，也不是事實。

「啊，是啊。」

哈特的話，給了萊斯力量。

這幾天，母親的樣子比以前更奇怪了。
她從戴著奇怪項圈的五年前開始就一直很煩躁，最近她卻愉快到令人作噁。

她有什麼陰謀。也許已經開始在做了。
不管怎麼說，已經毫無疑問這是與王國為敵的作為了。

是到了訣別的時候。

「即使對手是我母親，如果要擾亂國家的話，我都會收拾的。」

「不，你不行吧。從實力上來說。」

「那就痛快地沖走吧！」

無論如何，公主和王子都將加入卡美洛的行列。

（那麼，我該怎麼辦呢……）

雖然很麻煩，但是希望妹妹能享受。
哈特泡在溫泉裏，一邊制定下一步的對策——。