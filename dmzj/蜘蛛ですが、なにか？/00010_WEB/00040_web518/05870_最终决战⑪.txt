迟到了！

拉斯(京也)视角
===================

身体自己暴走著。
而我则像是第三者似地观察著。
就像灵魂脱离，俯瞰著自己身体一样。
可惜视野还是和身体原本看见的一样，没法透过俯瞰视角把握整体战场。
只有感觉是像灵魂脱离而已。

有空考虑这种无聊事情，是因为现在没我能做的事情了。
发动愤怒后，我的身体完全不受控制。
差一点就能是驾驶煞车坏掉的暴走车司机，然而我现在坐的是后座，甚至不是驾驶座。
外道无效也没办法彻底抵销愤怒的负面效果。
我的意识恐怕再也无法回到我的身体里面了吧。
所以我到死为止都只能这样看著。

但我并不后悔。
在妖精之里，没有愤怒的我赢不过两只古龙。
所以我才决定，如果要与古龙战斗，就果断地使用愤怒。
要说有悬念的话，就是发动愤怒后会无法区别敌我，但好险战斗开始时附近并没有自己人。
那剩下就只是尽情暴走而已。

不知道是第几次了，我把飞扑过来的豹龙砍断。
豹龙MP已经空了，只能雷也不缠地飞扑过来。
攻击而来的尖爪獠牙锐利无比，柔软的豹身动作轻盈。
如果对手只是普通人类，就算不用雷电，光凭能力值也能直接碾压吧。
但发动愤怒的我，能力值甚至超过全盛时期的爱丽儿。
能力值不如我的豹龙，只不过是个会动的靶子而已。

「唔喔喔喔喔喔！」

以那豹龙为饵，火龙从背后殴向我。
再用前脚向我挥下……没有挥下！？
火龙抓住我的身体。
并就这样随著冲击，连著我一起飞入岩浆。
身体在燃烧著。
就算我能力值再高，直接跳入岩浆中也不可能没有伤害。
伤害超过HP的自动回复，一点一点地累积。
但我的身体没有一丝慌乱，切断火龙抓住我的前脚，从岩浆中脱离。

从岩浆中跳出来后，等著我的是早已准备好的空气子弹。
空气子弹直击头部使大脑晃动。
就算能力值再高，这种攻击还是会造成伤害。
而且大脑晃动又带来了弊害。
因为空气子弹的冲击和脑震荡，我的身体再次落入岩浆之中。

多亏晕眩无效，脑震荡马上治好，我的身体再次跳出岩浆。
空气子弹再次飞来，但知道会来就很好处理了。
我挥动右手弹开空气子弹。
然后再挥下左手，打算砍断接近而来的翼手龙。
然而手中握著的刀，刀身已经没了。
反击成空，我的身体被第三次踢落岩浆。
是学习到从同个地方上岸有危险了吗，我的身体在岩浆中稍微游了下从别的地方上岸。

两手的刀都没了刀身。
就算我的身体能承受岩浆的酷热，武器也撑不住。
我丢掉那两把刀，从空间收纳取出预备的刀。
战斗前准备的魔剑几乎都用光了。
这预备的刀就是最后了。
然后我的HP也因为刚才的攻防而减少不少。

愤怒会使所有能力值变成十倍，但只有HP、MP、SP不在此限。
就算最大值增加，现有数值也还是保持愤怒发动时的数值，不会增加。
HP、MP还能依靠自动回复缓缓增加，但SP连这都没有。
然后被愤怒支配的我也不会保留MP。
MP快要没了。
HP也因为古龙们一次次的攻击，缓缓但确实地在减少。
不只HP。
我的能力值也因为不知道躲在哪的施术者一点一点减少。
好像是诅咒的负面效果。
虽然相当缓慢，但我确实一点一点地被逼近绝路。

古龙们再次攻过来。
那之中有著我刚才砍死的豹龙在。
视野的一角发现了俊。
这场战斗的关键是他。
因为他不停复活古龙，我才没办法取得胜利。

尽管我打倒古龙好几次，我却一次都没有升级。
打倒如此强大的古龙理应会升级。
大概是复活导致的特殊判定吧。
升级是获得系统回收灵魂时的溢出的部份能量，所以灵魂没有被系统回收就没办法升级。
一般经验值都是在打倒对手的瞬间获得，但附近有人可以复活时会延后这判定。
所以不打倒可以复活他人的俊，就算打倒再多古龙也拿不到任何经验值。

只要能先解决掉俊，战斗就会变得有利。然而被愤怒支配的我似乎优先攻击眼前的对手。
明知俊的麻烦却置之不理。
……不对，或许是我内心深处自己这样期望的也说不定。

应该早就做好觉悟了。
亲手杀掉过去朋友的觉悟。
但我内心某处却似乎还无法接受，所以才刻意不去这样做。
既然这样……。

「嘎啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊！！」

那份天真，就不得不舍弃掉。

「不妙！？」

突破古龙们的包围网，我冲向俊那边。
俊紧绷那现在也要倒下的苍白脸庞。
叶多阻挡在他之前。
她举起双手，用防护壁般的东西覆盖她自己和俊。
我拿刀向著那防护壁砍下，但居然被弹开了。
然而防护璧也出现了裂痕。
并且我是二刀流。
防护壁在我用另外一把刀砍下去后，彻底碎裂。

「哥哥大人！」

然而就在我被防护璧拖延的这一瞬间，有个人便赶到了我和俊之间。
身为俊今世妹妹的少女，借由小白龙的加护与古龙们一同和我互斗。
但那也是在有其他古龙的辅助下。
一对一就不是我的对手了。
我用双刀同时砍向俊的妹妹。
俊的妹妹虽然用剑接下这招，但踩不住脚的她就这样被弹飞到了后方。
这样就没有人守护俊了！

我举起双刀。
然后毫不犹豫地用力挥下。
俊突然推开叶多，挺身想保护她。
然而他能做到的也就这些。
他避不开我的攻击。
所以，我应该切裂了过去的友人才对。

「？」

眼前是无事的俊。
我不可思议地看向自己的手，手中双刀的刀身都断裂了。
MP用尽。
双刀的魔力附加效果因此没了。
没了魔力附加的武器，就算是魔剑也承受不住我的全力。

我身体的反应很快。
就这样用折断的刀向著俊刺出。
就算没有刀身也是我的突刺。
只要碰到脸就能杀掉俊。

但我的手腕被别人的手腕绑住。
视野旋转。

我一瞬间不知道发生了什么事情。
在旋转的视野中，我看见一直站在俊附近的男性(?)摆出了奇妙的姿势。
那是背负投的姿势。
而我好像就是吃下了这招。

我身体被抛向空中回旋，豹龙紧接而来咬向了我的喉咙。
诅咒的负面效果似乎让我防御力降低不少，豹龙的獠牙咬进了我的脖子。
被咬著著地的我，用力地殴打豹龙。
豹龙的身体被我打到地面，我被咬的脖子也如泉涌地喷血。

……不妙啊。
HP激烈减少。
武器没了。
MP也没了。

……到此为止了吗。

身体倒下。

「京也……」

别摆出这种表情啊，俊。

「别……表……」

哈哈，是气管也受创了吗，声音都发不好。
……嗯？
我靠自己的意识出声了？
要死了，所以愤怒解除了？
哈，哈哈哈！
这还真是侥幸！
那我要做的也决定好了。

「奉……献……！」

绝不能在这失败，我一字一音地确实说出口。
虽然一张嘴血就从嘴巴流出，但应该有好好说出来。

「！？你这是！？」

把我投出去的男子(？)大吃一惊。
俊和叶多则好像不理解我做了什么。

我从一开始便打算这样。
如果我在这场战斗死去，并且还解除了愤怒的话便打算这样做。
我在这世界杀了太多人。
此乃罪业。
罪业就必须接受责罚。
所以，就为这世界用尽我所有灵魂吧。
这就是我的赎罪。

白、爱丽儿、苏菲亚。
抱歉。
之后就交给你们了。
我就到此为止了。

然后，我化为尘埃消逝。